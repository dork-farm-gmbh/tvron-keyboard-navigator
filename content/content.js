
const getPlayerUrl = (channelId) => {
	switch (channelId) {
		case 'c-antena-1':
			return 'https://tvron.net/tv-hd/index.php?url=a1_hd_2';
		case 'c-kanal-d':
			return 'https://tvron.net/tvlive/index.php?url=kanal-d';
		case 'c-antena-3':
			return 'https://tvron.net/tvlive/index.php?url=a-3';
		case 'c-rtv':
			return 'https://tvron.net/tvlive/index.php?url=romania-tv';
		case 'c-look-plus':
			return 'https://tvron.net/tvlive/index.php?url=look-sport-plus';
		case 'c-realitatea-tv-plus':
			return 'https://tvron.net/tv-hd/index.php?url=realitatea-tv';
			case 'c-antena-stars': 
				return 'https://tvron.net/tvlive/index.php?url=a-stars-k';
			case 'c-antena-stars': 
				return 'https://tvron.net/tvlive/index.php?url=a-stars-k';
			case 'c-happy-channel': 
				return 'https://tvron.net/tv-hd/index.php?url=happy-channel_k';
			case 'c-protv-international': 
				return 'https://tvron.net/tvlive/index.php?url=pro-tv-international';
			// case '': 
			// 	return '';
		default:
			return `https://tvron.net/tvlive/index.php?url=${channelId.substring(2)}`;
	}
}

const highlightFirstChannel = () => {
	// remove header
	const header = document.getElementById('header');
	header.parentElement.removeChild(header);

	// remove right panel
	const rightPanel = document.getElementById('right');
	rightPanel.parentNode.removeChild(rightPanel);

	// click on first a in #favorite to set focus
	const fav = document.getElementById('left');
	fav.getElementsByTagName('a')[0].classList.add('focused');
}

function checkKey(e) {
	e.preventDefault();
	const focused = document.getElementsByClassName('focused')[0];
	let nextFocused = undefined

	// left arrow
	if (e.keyCode == '37') {
		console.log('left arrow');
		nextFocused = focused.previousSibling.previousSibling
	}
	// right arrow
	else if (e.keyCode == '39') {
		nextFocused = focused.nextSibling.nextSibling;
		// 
	} else if (e.keyCode == '13') {
		const url = getPlayerUrl(focused.id);
		window.location = url;
		// tab => home
	} else if (e.keyCode == '9') {
		window.location = `https://tvron.net/romanesti/`
	} else {
		return;
	}

	nextFocused.classList.add('focused');
	nextFocused.scrollIntoView({ block: "center" });

	focused.classList.remove('focused');
}


document.onkeydown = checkKey;

// on main website
if (document.location.host === "tvron.net") {
	switch (document.location.pathname) {
		// home page
		case '/romanesti/':
			// wait to load and select first channel
			setTimeout(() => {
				highlightFirstChannel();
			}, 500);

			break;
		default:
			setTimeout(() => {
				// click on play
				const playButton = document.getElementsByClassName('content-protector-form-submit')[0]
				if (playButton) {
					playButton.click();
				}

				// remove extra divs
				const contentDiv = document.getElementsByClassName('content')[0];
				for (let i = 0; i < 2; i++) {
					const extraDiv = document.getElementsByClassName('posts1')[0];
					contentDiv.removeChild(extraDiv);
				}

				// remove more extra divs
				const mainDiv = document.getElementsByClassName('main')[0];
				for (let i = 0; i < 4; i++) {
					const extraDiv = document.getElementsByClassName('deindex')[0];
					mainDiv.removeChild(extraDiv);
				}

			}, 1000);
			break;
	}
	// inside player iframe
} else {
	// console.log('inside iframe');
}


